import numpy as np
# For custom imputer
from sklearn.base import TransformerMixin
from sklearn.base import BaseEstimator
from sklearn.impute import SimpleImputer
# For the pipelines
from sklearn.pipeline import Pipeline
from sklearn.compose import ColumnTransformer
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import FunctionTransformer


# Creating the CustomImputer object

class CustomImputer(BaseEstimator, TransformerMixin):
    def __init__(self):
        self.means = {}
        
    def fit(self, X, y=None):
        means_ = X.iloc[:,3:].groupby('Trader').mean().mean().values
        for ind,col in enumerate(X.columns.values[4:]):
            self.means[col] = means_[ind]
        return self
    
    def transform(self, X, y=None):
        X_ = X.copy()
        X_mean_ = X.iloc[:,3:].groupby('Trader').mean()
        
        # Filling the missing values in X_mean with global means
        X_mean_ = X_mean_.fillna(value=self.means)
        
        # Filling the missing values Trader by Trader
        for col in X.columns.values[4:]:
            for trader in X_mean_.index.values:
                X_[col][X_['Trader']==trader] = X_[col][X_['Trader']==trader].fillna(X_mean_.loc[trader, col])
        
        return X_


# Creating function helper to create the adapted pipeline with a StandardScaler

def create_pipeline(train_X, ind_range = None):
    
    if not ind_range:
        ind_range = [i for i in range(4,39)]

    imputer_transformer = ColumnTransformer([
                                             ('imputer', CustomImputer(), train_X.columns.values)
                                            ])
    scaler_transformer = ColumnTransformer([
                                            ('scaler', StandardScaler(), ind_range)
                                           ])
    preprocess = Pipeline([
                           ('imputer_transformer', imputer_transformer),
                           ('scaler_transformer', scaler_transformer)
                          ])
    return preprocess


def create_pipeline_log(train_X,  log_cols, log_plus_one_cols): # all other columns are left untouched
    column_index = {}
    for i,column_name in enumerate(train_X.columns.values):
            column_index[column_name] = i

    log_transformer = FunctionTransformer(lambda x : np.log(x.astype('float64')+1e-4))
    log_plus_one_transformer = FunctionTransformer(lambda x : np.log1p(x.astype('float64')))
    log_columns_mask = [(col in log_cols) for col in train_X.columns.values]
    log_plus_one_columns_mask = [(col in log_plus_one_cols) for col in train_X.columns.values]
    imputer_transformer = ColumnTransformer([
                                             ('imputer', CustomImputer(), train_X.columns.values)
                                            ])
    log_transformer = ColumnTransformer([
                                ('log_transformer',log_transformer,log_columns_mask),
                                ('log+1_transfomer',log_plus_one_transformer,log_plus_one_columns_mask)
                               ], remainder='passthrough')

    s_b = len(log_cols)+len(log_plus_one_cols)
    is_string = [i for i in range(s_b, s_b+4)]
    scaler_transformer = ColumnTransformer([
                                            ('scaler', StandardScaler(), [(i not in is_string) for i in range(41)])
                                           ])
    preprocess = Pipeline([
                           ('imputer_transformer', imputer_transformer),
                           ('log_transformer', log_transformer),
                           ('scaler_transformer', scaler_transformer)
                          ])
    return preprocess

def create_pipeline_log_select(train_X,  log_cols, log_plus_one_cols, identity_cols): # all other columns are removed
    column_index = {}
    for i,column_name in enumerate(train_X.columns.values):
            column_index[column_name] = i
    identity_transformer = FunctionTransformer(lambda x : x)
    log_transformer = FunctionTransformer(lambda x : np.log(x.astype('float64')+1e-4))
    log_plus_one_transformer = FunctionTransformer(lambda x : np.log1p(x.astype('float64')))
    
    identity_columns_mask = [(col in identity_cols) for col in train_X.columns.values]
    log_columns_mask = [(col in log_cols) for col in train_X.columns.values]
    log_plus_one_columns_mask = [(col in log_plus_one_cols) for col in train_X.columns.values]
    imputer_transformer = ColumnTransformer([
                                             ('imputer', CustomImputer(), train_X.columns.values)
                                            ])
    log_transformer = ColumnTransformer([
                                ('identity_transformer',identity_transformer,identity_columns_mask),
                                ('log_transformer',log_transformer,log_columns_mask),
                                ('log+1_transfomer',log_plus_one_transformer,log_plus_one_columns_mask)
                               ])

    s_b = len(log_cols)+len(log_plus_one_cols)+len(identity_cols)
    is_string = [i for i in range(s_b, s_b+4)]
    scaler_transformer = ColumnTransformer([
                                            ('scaler', StandardScaler(), [(i not in is_string) for i in range(s_b)])
                                           ])
    preprocess = Pipeline([
                           ('imputer_transformer', imputer_transformer),
                           ('log_transformer', log_transformer),
                           ('scaler_transformer', scaler_transformer)
                          ])
    return preprocess


