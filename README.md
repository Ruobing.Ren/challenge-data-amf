# Data Challenge AMF 2021
*Bruno Hays, Paul-Henri Behague, Ruobing Ren*

**AMF_test_Y_xxx.csv** <br>
Submission files we use to get the accuracy on the test set. 

**model_xxx** <br>
Model of the classifier we use to train and predict the dataset.

**Model_Benchmark_RF.ipynb** <br>
Notebook in which we try to reproduce the benchmark given. We apply the most basic preprocessing, and then find the best parameters for the Random Forest before using the given decision function.

**CustomPreprocess.py** <br>
Utils file which holds the custom-made Imputer for filling the missing values, aswell as some functions to help creating the different preprocessing pipelines.

**TestHelper.py** <br>
Utils file made to evaluate the different models more easily, and apply the decision function after the first prediction.

**Data_filling_nans.ipynb** <br>
Notebook in which we explore the different possibilites for the filling, and then implement it in a reusable class.

**Model_Benchmark_RF_Filled.ipynb** <br>
Notebook which builds the same model as in 'Model_Benchmark_RF', but with the filling method found and implemented in 'Data_filling_nans'.

**Data_exploration.ipynb** <br>
Contains all the visualization of each feature. Explains the transformations and extractions we used on the features

**Model_Exploration_RF.ipynb** <br>
Random Forest model using the conclusion of the data exploration and the correlation analysis. Data preprocessing is thus optimized.

**Data_correlation.ipynb** <br>
In this notebook, we explore the correlation between features and the output to get the list of features to drop. <br>
-If the features are strongly correlated between them, we keep only one of them, as the others don't bring additional interesting information. <br>
-If the feature is weakly correlated to the output, we delete it.

**Model_Correlation_RF.ipynb** <br>
Application of the results found in Data_correlation to evaluate our model. We use the Random Forest with the same parameters found before.

**Model_PCA_RF.ipynb** <br>
Random forest using the conclusion of the data exploration and applying PCA before training. Also contains vizualization of the PCA.

**Model_LDA_QDA.ipynb** <br>
Notebook in which we visualize the Dimensionnality Reduction with LDA, and also implement and train 2 classifiers with LDA and QDA.

**Model_LR_SVC.ipynb** <br>
In this notebook, we explore 2 simple models : Multimonial Logistic Regression and Support Vector Classification. 
After using the grid search method to find the optimal hyperparameters, we train these models on our training set and finally submit it to evaluate its performances. 

**Model_DecisionTree.ipynb** <br>
Exploring the simple decision tree. 

**Model_Boosting.ipynb** <br>
In this notebook, we explore 2 models of boosting : GBM and XGBoost.
After using the grid search method to find the optimal hyperparameters, we train these models on our training set and finally submit it to evaluate its performances. 

**Data_exploration_correlation.ipynb** <br>
Like Data_correlation (see above) but we modified the data according to the conclusions of the data exploration.

**Model_XGBoost-Explo-Corr.ipynb** <br>
Application of the new data preprocessing found by exploring the data with XGBoost.

**Model_Learning_Decision_Function.ipynb** <br>
Notebook in which we try learning the Decision Function (instead of using the given one), and using it after the first predictions made by the Benchmark model.



