from sklearn import metrics
import pandas as pd
import numpy as np


def test(X_test, y_test, pipeline):
    
    # Prediction
    y_pred = pipeline.predict(X_test)
    
    # Data
    pred_df = X_test['Trader'].to_frame()
    pred_df['type'] = y_pred

    real_df = X_test['Trader'].to_frame()
    real_df['type'] = y_test

    # Applying decision function
    real_traders = decision_ini(real_df)
    pred_traders = decision_ini(pred_df)
    
    return real_traders, pred_traders


def evaluate_f1(real, pred):
    return micro_average_f1_score(real, pred)


def confusion_matrix(real, pred, labels):
    return metrics.confusion_matrix(real.type.values, pred.type.values, labels=labels)
    
    
def decision_ini(df):
    
    # In this decision fonction : 
    ## a market player whose at least 85% of his rows are predicted as HFT is an HFT participant,
    ## a market player whose at least 50% of his rows are predicted as MIX is a MIX participant, 
    ## otherwise the model considers that the market player is a NON HFT.

    # df should contain :
    ## a column called "Trader"
    ## a column called "type" which could be the real type or the types predicted for the market player
    
    traders_list = np.unique(list(df['Trader']))
    traders_type = []
    for trader in traders_list:
        preds = list(df[df['Trader'] == trader]['type']) 
        non_hft = preds.count('NON HFT')
        mix = preds.count('MIX')
        hft = preds.count('HFT')
        rows = hft + non_hft + mix
        if hft/rows >= 0.85:
            traders_type.append('HFT')
        elif mix/rows >= 0.50:
            traders_type.append('MIX')
        else:
            traders_type.append('NON HFT')
    return pd.DataFrame({'Trader':traders_list, 'type':traders_type})


def micro_average_f1_score(dataframe_y_true, dataframe_y_pred):
    """
    Args
        dataframe_y_true: Pandas Dataframe
            Dataframe containing the true values of y.
            This dataframe was obtained by reading a csv file with following instruction:
            dataframe_y_true = pd.read_csv(CSV_1_FILE_PATH, index_col=0, sep=',')

        dataframe_y_pred: Pandas Dataframe
            This dataframe was obtained by reading a csv file with following instruction:
            dataframe_y_pred = pd.read_csv(CSV_2_FILE_PATH, index_col=0, sep=',')

    Returns
        score: Float
            The metric evaluated with the two dataframes. This must not be NaN.
    """

    score = metrics.f1_score(dataframe_y_true["type"], dataframe_y_pred["type"], average = "micro")

    return score